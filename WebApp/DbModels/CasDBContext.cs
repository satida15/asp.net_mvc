﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.DbModels
{
    public class CasDBContext : DbContext
    {
        public CasDBContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Cas> Cas { get; set; }
    }
}

