﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.DbModels;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class CasController : Controller
    {
        private readonly CasDBContext _context;

        public CasController(CasDBContext context)
        {
            _context = context;
        }

        // GET: Cas
        //Search: Cas
        public async Task<IActionResult> Index(string search)
        {
            var car = from c in _context.Cas
                        select c;

            if (!String.IsNullOrEmpty(search))
            {
                car = car.Where(b => b.Name.ToLower().Contains(search.ToLower()) || b.Title.ToLower().Contains(search.ToLower()) || b.Price.Contains(search));
            }
            return View(await car.ToListAsync());
        }

            // GET: Cas/Details/5
            public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cas = await _context.Cas
                .FirstOrDefaultAsync(m => m.ID == id);
            if (cas == null)
            {
                return NotFound();
            }

            return View(cas);
        }

        // GET: Cas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Title,Price")] Cas cas)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cas);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cas);
        }

        // GET: Cas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cas = await _context.Cas.FindAsync(id);
            if (cas == null)
            {
                return NotFound();
            }
            return View(cas);
        }

        // POST: Cas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Title,Price")] Cas cas)
        {
            if (id != cas.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CasExists(cas.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cas);
        }

        // GET: Cas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cas = await _context.Cas
                .FirstOrDefaultAsync(m => m.ID == id);
            if (cas == null)
            {
                return NotFound();
            }

            return View(cas);
        }

        // POST: Cas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cas = await _context.Cas.FindAsync(id);
            _context.Cas.Remove(cas);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CasExists(int id)
        {
            return _context.Cas.Any(e => e.ID == id);
        }
    }
}
