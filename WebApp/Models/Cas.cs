﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Cas
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Price { get; set; }


    }
}
